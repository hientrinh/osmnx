from snowflake.snowpark import Session
from snowflake.snowpark.functions import col
from config import connection_parameters



def main():
    session = Session.builder.configs(connection_parameters).create()
    udf = session.udf.register_from_file(
        file_path="osm_udf.py",
        func_name="route",
        name="lat_lon_udf",
        is_permanent=False,
        imports=["./osmnx/"],  
        replace=True
    )

    node = session.table("OSM_NEW_YORK.NEW_YORK.V_OSM_NY_NODE")
    p = node \
        .select("ID", "LAT", "LON", udf("LON", "LAT").alias("nearest_nodes")) \
        .where((col("ID") == '9619287270') | (col("ID") == '9619756591'))

    p.write.saveAsTable("LAT_LON_TEST", mode="overwrite")

if __name__ == "__main__":
    main()




        

