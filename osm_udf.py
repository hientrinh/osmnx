import osmnx as ox

def route(LON: float, LAT: float) -> int:
    # download/model a street network for some city 
    G = ox.graph_from_place("New York, New York, USA", network_type="drive")

    # impute missing edge speeds and calculate edge travel times with the speed module
    G = ox.speed.add_edge_speeds(G)
    G = ox.speed.add_edge_travel_times(G)


    # get the nearest network nodes to two lat/lng points with the distance module
    nearest_nodes = ox.distance.nearest_nodes(G, X=LON, Y=LAT)
    
    # print(edge_lengths)
    return nearest_nodes